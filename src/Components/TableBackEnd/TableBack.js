import React from "react";
import axios from "axios";
import './TableBack.css'


export default class TableBack extends React.Component {

    state = {
        dataArr: [], // Массив полученых данных с сервера
        divShow: false,
        divEditShow: false,
        name: '',
        age: '',
        email: '',
        btnEditShow: true,
        idData: '',
    };

    // Функция запроса на получения данных с сервера
    getData = async () => {
        const dataBackEnd = await axios.get('http://178.128.196.163:3000/api/records').catch(err => console.log(err));
        this.setState({ dataArr: dataBackEnd.data })
        console.log(this.state.dataArr);
    }
    componentDidMount() {
        this.getData();
    }

    // Функция делает блок добавления данных ВИДИМЫМ
    addNewData = () => {
        this.setState({ divShow: true })
    }

    // Ряд функций, которые меняют стейты ==================
    nameHandleChange = e => {
        this.setState({ name: e.target.value });
    }

    ageHandleChange = e => {
        this.setState({ age: e.target.value });
    }

    emailHandleChange = e => {
        this.setState({ email: e.target.value });
    }
    // ==================


    //Функция отправляет запрос добавления данных на сервер
    putData = async () => {
        const data = {
            name: this.state.name,
            age: this.state.age,
            email: this.state.email
        }
        await axios.put(`http://178.128.196.163:3000/api/records`, { data }).catch(err => console.log(err))
    }
    addNoteOnPage = () => {
        this.putData();

        setTimeout(() => {
            window.location.reload();
        }, 500);
        this.setState({
            divShow: false,
            name: '',
            age: '',
            email: ''
        })

    }

    //Функция отправляет запрос удаления данных с сервера
    delData = async (e) => {
        await axios.delete(`http://178.128.196.163:3000/api/records/${e.target.id}`).catch(err => console.log(err))
    }
    deleteData = e => {
        this.delData(e);

        setTimeout(() => {
            window.location.reload();
        }, 500);
    }
    //Функция отправляет запрос обновления данных с сервера
    postData = async (e) => {
        // if()
        const data = {
            name: this.state.name,
            age: this.state.age,
            email: this.state.email
        }
        await axios.post(`http://178.128.196.163:3000/api/records/${e.target.id}`, { data }).catch(err => console.log(err))
    }

    btnShowSaveInputs = (e) => {
        this.postData(e);
        setTimeout(() => {
            window.location.reload();
        }, 500);
        this.setState({
            idData: ''
        })
    }

    btnShowEditInputs = (e) => {
        // console.log(e.data.id)
        this.setState({
            idData: e.target.id,
            divEditShow: true
        })
    }


    render() {
        const { dataArr, divShow } = this.state

        return (
            <div className='containerTableBack' >
                <div>
                    <table className="table tableBack" border="1">
                        <caption>Here is data backEnd:</caption>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            {dataArr.map(e => {
                                // console.log(e._id)
                                if (!e.data) {
                                    return <tr key={e}><td>Неизвестно</td><td>Неизвестно</td><td>Неизвестно</td>
                                        <button
                                            // key={e._id}
                                            id={e._id}
                                            className={e._id === this.state.idData ? '' : 'btnShow'}
                                            onClick={this.btnShowSaveInputs}
                                        >Save</button>
                                        <button
                                            // key={e._id}
                                            id={e._id}
                                            className={e._id === this.state.idData ? 'btnShow' : ''}
                                            onClick={this.btnShowEditInputs}
                                        >Edit</button>
                                        <button
                                            id={e._id}
                                            onClick={this.deleteData}
                                        >Delete</button> </tr>
                                } else {
                                    return (
                                        <tr key={e}>
                                            <td>
                                                <input
                                                    id={e._id}
                                                    type='text'
                                                    onChange={this.nameHandleChange}
                                                    value={e._id === this.state.idData ? null : e.data.name}
                                                    readOnly={e._id === this.state.idData ? '' : 'readonly'}
                                                /></td>
                                            <td>
                                                <input
                                                    id={e._id}
                                                    type='number'
                                                    onChange={this.ageHandleChange}
                                                    value={e._id === this.state.idData ? null : e.data.age}
                                                    readOnly={e._id === this.state.idData ? '' : 'readonly'}
                                                /></td>
                                            <td>
                                                <input
                                                    id={e._id}
                                                    type='text'
                                                    onChange={this.emailHandleChange}
                                                    value={e._id === this.state.idData ? null : e.data.email}
                                                    readOnly={e._id === this.state.idData ? '' : 'readonly'}
                                                /></td>
                                            <button
                                                // key={e._id}
                                                id={e._id}
                                                className={e._id === this.state.idData ? '' : 'btnShow'}
                                                onClick={this.btnShowSaveInputs}
                                            >Save</button>
                                            <button
                                                // key={e._id}
                                                id={e._id}
                                                className={e._id === this.state.idData ? 'btnShow' : ''}
                                                onClick={this.btnShowEditInputs}
                                            >Edit</button>
                                            <button
                                                id={e._id}
                                                onClick={this.deleteData}
                                            >Delete</button> </tr>
                                    );
                                }
                            })}
                        </tbody>
                    </table>
                </div>

                <div className={`newData-Div ${!divShow ? 'displayDivNone' : ''} `} >
                    <input placeholder="Name" type='text' onChange={this.nameHandleChange} />
                    <input placeholder="Age" type='number' onChange={this.ageHandleChange} />
                    <input placeholder="Email" type='text' onChange={this.emailHandleChange} />
                    <button onClick={this.addNoteOnPage} >Add Note</button>
                </div>

                <button onClick={this.addNewData} >Add Note</button>

            </div>
        );
    }
}