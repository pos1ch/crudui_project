import React, { useState } from "react";
import data from './data.json';
import './Table.css'


function Table() {
    const [new_Name, setNewName] = useState('');
    const [new_Email, setNewEmail] = useState('');
    const [new_Body, setNewBody] = useState('');


    const [div_Show, setDivShow] = useState(false)

    const addNewData = () => {
        setDivShow(true);
    }

    const newNameHandler = (e) => {
        setNewName(e.target.value);
    }

    const newEmailHandler = (e) => {
        setNewEmail(e.target.value);
    }

    const newBodyHandler = (e) => {
        setNewBody(e.target.value);
    }

    const addNoteOnPage = () => {
        if (new_Name !== '' && new_Email !== '' && new_Body !== '') {
            setDivShow(false);
            data.push({
                "name": new_Name,
                "email": new_Email,
                "body": new_Body
            })
            // console.log(data);
            setNewName('');
            setNewEmail('');
            setNewBody('');
        } else {
            alert('Empty inputs');
        }
    }
    // console.log(data);


    return (
        <div>
            <table className="table" border="1">
                <caption>Таблица с фейковыми данными</caption>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Body</th>
                </tr>
                {Object.keys(data).map(e => {
                    return (
                        <tr key={e}><td>{data[e].name}</td><td>{data[e].email}</td><td>{`${(data[e].body.length > 100) ? data[e].body.slice(0, 100) + '...' : data[e].body}`}</td></tr>
                    );
                })}
            </table>

            <div className={`newData-Div ${!div_Show ? 'displayDivNone' : ''} `} >
                <input placeholder="Name" type='text' onChange={e => newNameHandler(e)} value={new_Name} />
                <input placeholder="Email" type='text' onChange={e => newEmailHandler(e)} value={new_Email} />
                <input placeholder="Body" type='text' onChange={e => newBodyHandler(e)} value={new_Body} />
                <button onClick={addNoteOnPage} >Add Note</button>
            </div>

            <button onClick={addNewData} >Add Note</button>
        </div>
    );
}


export default Table;
