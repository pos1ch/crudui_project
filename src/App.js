import React, { Component } from 'react';
import './App.css';
import Table from './Components/Table/Table';
import TableBack from './Components/TableBackEnd/TableBack.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Table />
        <TableBack />
      </div>
    );
  }
}

export default App;
